#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 16:51:56 2019

@author: mhschwade
"""
# Esse script calcula a transmissão para a água - amostras de terc-butanol.

# Importa as bibliotecas.
import numpy as np
import matplotlib.pyplot as plt

# Importa os dados de intensidade transmitida no vácuo.
intvac = np.loadtxt("intensity_vacuo.dat", unpack=True)

# Média ponderada (pelo tempo) da intensidade transmitida no vácuo. 
media_intvac = 0
tot = 0
for i in range(10):
    if i<5:
        media_intvac += intvac[i]*1.
        tot += 1.
    elif i>=5:
        media_intvac += intvac[i]*10.
        tot += 10.
media_intvac /= tot

print(media_intvac)

#

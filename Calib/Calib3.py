import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import linregress


############### Define uma classe com o formato dos dados do SAXS. #########################
############################################################################################

class Saxs():
    q = np.zeros(502)
    I = np.zeros(502)
    sq = np.zeros(502)
    lnI = np.zeros(502)
    
############################################################################################


###################### Importa os dados de x0 do ajuste dos picos. #########################
############################################################################################

def ImportPeaks(arquivo):
    peak_param = np.loadtxt(arquivo, unpack=False)

    return peak_param[0]

############################################################################################


################## Importa os dados de média do padrão já computados. ######################
############################################################################################

def ImportData(input):
    dados = Saxs()
    dados.q, dados.I, dados.sq = np.loadtxt(input, usecols=(0,1,2), unpack=True)
    for i in range(501):
        if np.log(dados.I[i]) > -12:
            dados.lnI[i] = np.log(dados.I[i])
        else:
            dados.lnI[i] = -12 #min(dados.lnI)
            
    return dados

############################################################################################


########################### Faz o gráfico do ajuste linear. ###############################
###########################################################################################

def PlotFit2(x, y, coef):
    xcal = np.linspace(0, 0.40, 100)
    ycal = coef[0]*xcal + coef[1]

    plt.plot(x, y, linestyle='', marker='x', color='#bb0022', markersize=4, label=u'Dados da calibração')
    plt.plot(xcal, ycal, linestyle='--', color='#0077cc', label=u'Curva de calibração')
    plt.legend(loc='upper left')
    plt.grid(True)
    plt.xlabel(r'$q \quad (\AA^{-1})$')
    plt.ylabel(r'$q_{calibrado} \quad (\AA^{-1})$')
    plt.xlim(0,0.38)
    #ybot, ytop = plt.ylim()
    plt.xticks(np.arange(0, 0.40, 0.05))
    plt.yticks(np.arange(0, 0.40, 0.05))
    plt.tick_params(axis='x', labelsize=8)
    plt.tick_params(axis='y', labelsize=8)
    plt.title(u'Calibração dos picos')
    plt.savefig('Peak_Calib.pdf')
    plt.show()

    return 0

##############################################################################################



############## Faz o ajuste linear para calibrar os picos e salva em arquivo. ##############
############################################################################################

def LinFit(x, y, saida):
    coef, cov = np.polyfit(x, y, 1, cov=True)

    arqfit = saida + ".dat"
    with open(arqfit, 'w') as arq:
        arq.write("# Resultado do ajuste de calibração dos picos.\n")
        arq.write("# Picos de referência (A-1).\n")
        arq.write("%.5e\t %.5e\t %.5e\n" % (y[0], y[1], y[2]))
        arq.write("# Picos ajustados (A-1).\n")
        arq.write("%.5e\t %.5e\t %.5e\n" % (x[0], x[1], x[2]))
        arq.write("# Coeficientes do ajuste linear (y = ax + b): [a b].\n")
        arq.write("%.5e\t%.5e\n" % (coef[0], coef[1]))
        arq.write("# Matriz de covariância.\n")
        arq.write("%.4e\t%.4e\n%.4e\t%.4e\n" % (cov[0][0], cov[0][1], cov[1][0], cov[1][1]))

    return coef, cov

############################################################################################
################################## PROGRAMA PRINCIPAL ######################################
############################################################################################

# Define os picos de referência.
peak_ref = np.array([0.108, 0.216, 0.323])


# Importa os dados dos picos.
entrada = input("Entre com o arquivo de dados dos picos:")
peaks = ImportPeaks(entrada)

# Define o arquivo de saída da calibração.
saida = input("Entre com o nome do arquivo de saida (sem terminação):")

# Faz o ajuste de calibração: primeiro picos ajustados, depois os picos de referência, e salva o resultado.
coef, cov = LinFit(peaks, peak_ref, saida)

# Faz o gráfico do ajuste de calibração.
PlotFit2(peaks, peak_ref, coef)



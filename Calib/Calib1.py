import numpy as np
import matplotlib.pyplot as plt

# Define uma classe com o formato dos dados do saxs.
class Saxs():
    q = np.zeros(502)
    I = np.zeros(502)
    sq = np.zeros(502)
    lnI = np.zeros(502)
    
def Init(input):
    arq = open(input,'r')
    
    lines = arq.readlines()

    linha_base = lines[1].split()
    narq = int(linha_base[1])
    linha = [[] for _ in range(narq)]
    for i in range(narq):
         linha[i] = lines[i+2].split()
         linha[i][0] = linha_base[0] + linha[i][0]

    arq.close()

    # Importa os dados dos arquivos de dados, a partir da linha 31.
    dados = [[] for _ in range(narq)]
    for i in range(narq):
        arqdados = linha[i][0]
        q, I, sq = np.loadtxt(arqdados, skiprows=30, usecols=(0,1,2), unpack=True)
        dados[i] = [float(linha[i][1]), q, I, sq]

    return dados

# Faz a média dos dados obtidos com o SAXS.
def Media(dados, saida):
    narq = len(dados)
    #tam = len(dados[0][1])
    #print(tam)
    data = Saxs()
    temp_tot = 0
    for i in range(narq):
        temp_tot += dados[i][0]
        
        
    for i in range(narq):
        p = dados[i][0]/temp_tot
        data.I += dados[i][2]*p
        data.sq += (dados[i][3]*p)**2
        
    data.lnI = np.log(data.I)
    data.sq = np.sqrt(data.sq)
    data.q = dados[0][1]
    with open(saida + ".dat", 'w') as f:
        f.write("# Médias dos dados para calibração\n")
        tam = len(data.q)
        for i in range(tam):
            f.write("%.6e\t %.6e\t %.6e\n" % (data.q[i], data.I[i], data.sq[i]))
    return data

# Plota o logaritmo da intensidade vs vetor de espalhamento.
def Plot(data, saida):
#    erro = data.sq/data.I
#    plt.errorbar(data.q, data.lnI, yerr=erro, linestyle='', marker='x', color='#11bbff', markersize=1, label=r'log(I)')
    plt.plot(data.q, data.lnI, linestyle='', marker='x', color='#11bbff', markersize=1, label=r'log(I)')
    plt.legend(loc='upper right')
    plt.grid(True)
    plt.xlabel(r'$q \quad (\AA^{-1})$')
    plt.ylabel(r'log(I)')
    plt.xlim(0,0.38)
    #ybot, ytop = plt.ylim()
    plt.xticks(np.arange(0, 0.40, 0.02))
    #plt.yticks(np.arange(ybot, ytop, 1))
    plt.tick_params(axis='x', labelsize=6)
    plt.tick_params(axis='y', labelsize=8)
    plt.title(r'Logaritmo da Intensidade $\times$ Vetor de Espalhamento')
    plt.savefig(saida + ".pdf")
    #plt.tight_layout()
    plt.show()
    return 0

############################### Programa Principal ###############################
    
entrada = input("Entre com a lista de arquivos:") #'lista_calibracao.dat'
saida = input("Entre com o nome do arquivo de saída (sem terminação):")
dados = Init(entrada)
data = Media(dados, saida)
Plot(data, saida)

    

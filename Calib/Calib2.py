import matplotlib.pyplot as plt
import scipy.optimize as sco
import numpy as np

############################################################################################
############### Define uma classe com o formato dos dados do SAXS. #########################
class Saxs():
    q = np.zeros(502)
    I = np.zeros(502)
    sq = np.zeros(502)
    lnI = np.zeros(502)
    
############################################################################################


################## Importa os dados de média do padrão já computados. ######################
############################################################################################

def ImportData(inp):
    dados = Saxs()
    dados.q, dados.I, dados.sq = np.loadtxt(inp, usecols=(0,1,2), unpack=True)
    for i in range(501):
        if np.log(dados.I[i]) > -12:
            dados.lnI[i] = np.log(dados.I[i])
        else:
            dados.lnI[i] = -12 #min(dados.lnI)
            
    return dados

############################################################################################


################# Constrói o arquivo com os intervalos para ajuste. ########################
############################################################################################

def MakeRange(output):
    a = [[] for _ in range(3)]
    arq = open(output, 'w')
    arq.write("# Intervalos para ajuste dos picos\n")
    for j in range(3):  
        a[j].append(input("Intervalo do %d ° pico (qi:qf):\n" % (j+1)))
        a[j] = a[j][0].split(':')
        for i in range(2):
            a[j][i] = float(a[j][i])
        arq.write("# Pico %d\n" % (j))
        arq.write("%.5f\t %.5f\n" % (a[j][0], a[j][1]))
    arq.close()
    return a

############################################################################################


################ Recebe os dados dos intervalos para ajuste dos picos. #####################
############################################################################################

def ReadRange(intervalo):
    qrange = np.loadtxt(intervalo, unpack=False)
    return qrange

############################################################################################


############ Define uma função lorentziana com deslocamento vertical. ######################
############################################################################################

def lorentz(x, x0, y, I, Imin):
    f = I/(1+((x-x0)/y)**2) - Imin
    return f

############################################################################################


################ Faz o ajuste de uma lorentziana nos picos. ################################
############################################################################################

def Ajuste1(qran, dados):

    # Constrói os vetores do intervalo de ajuste. ###########################################
    tam = 0
    init = 0
    for i in range(501):
        if dados.q[i]>qran[0] and dados.q[i]<qran[1]:
            if tam==0:
                init = i
            tam += 1

    xr = np.zeros(tam)
    yr = np.zeros(tam)
    for i in range(tam):
        xr[i] = dados.q[init+i]
        yr[i] = dados.lnI[init+i]

    # Chute inicial dos parâmetros. #########################################################
    params = []
    params.append(0.5*(max(xr) + min(xr)))
    params.append(7.0e-3)
    params.append(max(yr) - min(yr))
    params.append(min(yr))

    # Ajusta a curva. #######################################################################
    params, cov = sco.curve_fit(lorentz, xr, yr, method='lm', p0=params)
    
    print(params)
    print("\n")
    print(cov)
    print("\n")

    # Cria lista de parâmetros+variância para o ajuste. #####################################
    ajus = []
    for j in range(4):
        ajus.append([params[j], cov[j][j]])

    # Retorna os parâmetros com suas variâncias.
    return ajus, xr

##############################################################################################


################ Plota os ajustes junto com os dados de espalhamento. ########################
##############################################################################################

def PlotFit(data, xa1, ya1, xa2, ya2, xa3, ya3):
    plt.plot(data.q, data.lnI, linestyle='', marker='x', color='#11bbff', markersize=1, label=r'log(I)')
    plt.plot(xa1, ya1, linestyle='-', color='#AA0000', label=r'Ajuste Pico 1', linewidth=1)
    plt.plot(xa2, ya2, linestyle='-', color='#AA00AA', label=r'Ajuste Pico 2', linewidth=1)
    plt.plot(xa3, ya3, linestyle='-', color='#0000AA', label=r'Ajuste Pico 3', linewidth=1)
    plt.legend(loc='upper right')
    plt.grid(True)
    plt.xlabel(r'$q \quad (\AA^{-1})$')
    plt.ylabel(r'log(I)')
    plt.xlim(0,0.38)
    #ybot, ytop = plt.ylim()
    plt.xticks(np.arange(0, 0.40, 0.02))
    #plt.yticks(np.arange(ybot, ytop, 1))
    plt.tick_params(axis='x', labelsize=6)
    plt.tick_params(axis='y', labelsize=8)
    plt.title(r'Ajuste dos picos')
    plt.savefig('Peak_Fit.pdf')
    plt.show()
    return 0

##############################################################################################


######### Faz o ajuste dos picos para calibração e salva o resultado em um arquivo. ##########
##############################################################################################

def Fitting1(dados, fit_ranges, output):

    # Importa os dados de intervalos de ajuste. ##############################################
    qran = ReadRange(fit_ranges)

    
    # Faz os ajustes. ########################################################################
    
    # Ajuste para o primeiro pico.
    param1, xa1 = Ajuste1(qran[0], dados)

    # Ajuste para o segundo pico.
    param2, xa2 = Ajuste1(qran[1], dados)

    # Ajuste para o terceiro pico.
    param3, xa3 = Ajuste1(qran[2], dados)
    

    # Salva os parâmetros do ajuste. ##########################################################

    arq2 = open(output, 'w')

    arq2.write("# Parâmetros do ajuste dos picos\n")
    arq2.write("# Pico 1\t Pico 2\t Pico 3\n")

    arq2.write("# x0\n%.4e\t %.4e\t %.4e\n" % (param1[0][0], param2[0][0], param3[0][0]))
    arq2.write("# var(x0)\n%.3e\t %.3e\t %.3e\n" % (param1[0][1], param2[0][1], param3[0][1]))
    arq2.write("# y\n%.4e\t %.4e\t %.4e\n" % (param1[1][0], param2[1][0], param3[1][0]))
    arq2.write("# var(y)\n%.3e\t %.3e\t %.3e\n" % (param1[1][1], param2[1][1], param3[1][1]))
    arq2.write("# I\n%.4e\t %.4e\t %.4e\n" % (param1[2][0], param2[2][0], param3[2][0]))
    arq2.write("# var(I)\n%.3e\t %.3e\t %.3e\n" % (param1[2][1], param2[2][1], param3[2][1]))
    arq2.write("# Imin\n%.4e\t %.4e\t %.4e\n" % (param1[3][0], param2[3][0], param3[3][0]))
    arq2.write("# var(Imin)\n%.3e\t %.3e\t %.3e\n" % (param1[3][1], param2[3][1], param3[3][1]))
    
    arq2.close()

    # Grafica os resultados do ajuste.
    ya1 = lorentz(xa1, param1[0][0], param1[1][0], param1[2][0], param1[3][0])
    ya2 = lorentz(xa2, param2[0][0], param2[1][0], param2[2][0], param2[3][0])
    ya3 = lorentz(xa3, param3[0][0], param3[1][0], param3[2][0], param3[3][0])
    PlotFit(dados, xa1, ya1, xa2, ya2, xa3, ya3)
    
    return 0
    
###############################################################################################
###############################################################################################


################################### PROGRAMA PRINCIPAL ########################################
###############################################################################################

entrada = input("Entre com o arquivo de médias (saída '.dat' do primeiro programa):")
saida = input("Entre com o arquivo de saída (sem terminação):")

# Importa os dados. ###########################################################################
dados = ImportData(entrada)


# Cria um arquivo de dados com os valores de intervalos de ajuste. ############################
MakeRange(saida + "_PeakRanges.dat")


# Ajusta os picos para calibração. ############################################################
Fitting1(dados, saida + "_PeakRanges.dat", saida + "_Parameters.dat")

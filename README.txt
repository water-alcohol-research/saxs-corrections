Estes programas podem ser utilizados para realizar o tratamento de dados\
e as correções de dados de Espalhamento de Raios X a Baixo Ângulo (SAXS).\
A documentação desse conjunto de programas ainda está em processo de construção\
e os programas estão sujeitos a alterações para otimização do seu funcionamento\
e utilização. Fique atento. Em caso de dúvidas entre em contato com o autor.

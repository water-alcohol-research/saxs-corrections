import numpy as np
import matplotlib.pyplot as plt
#from scipy.stats import linregress


############### Define uma classe com o formato dos dados do SAXS. #########################
############################################################################################

class Saxs():
    def __init__(self, Nq):
        self.q = np.zeros(Nq)
        self.I = np.zeros(Nq)
        self.sI = np.zeros(Nq)
        self.lnI = np.zeros(Nq)
    
############################################################################################


###################### Importa os parâmetros e nomes de arquivos. ##########################
############################################################################################

def ImportParameter(arquivo):
    # Extrai os dados do arquivo ignorando as linhas comentadas.
    with open(arquivo, 'r') as f:
        lines = []
        for linha in f:
            if (linha[0] != '#' and linha[0] != '\n' and linha[0] != ' ' and linha[0] != '\t'):
                lines.append(linha)

    # Verifica se o número de linhas é compatível com o esperado, interrompe o processo se não.
    if (len(lines) != 10):
        print("Problema com o arquivo de parâmetros!\n")
        

    # Obtêm os parâmetros e retorna.
    aux = lines[0].split()
    a = float(aux[2])

    aux = lines[1].split()
    b = float(aux[2])

    arqAm = str(lines[2].strip('\n'))

    TrAm = float(lines[3])

    fr = float(lines[4])

    thAm = float(lines[5])

    arqCap = str(lines[6].strip('\n'))

    TrCap = float(lines[7])

    arqWater_cor = str(lines[8].strip('\n'))

    Norm = float(lines[9])
    
    return (a, b, arqAm, TrAm, fr, thAm, arqCap, TrCap, arqWater_cor, Norm)

############################################################################################


################## Importa os dados de média do já computados. #############################
############################################################################################

def ImportData(entrada):
    dados = Saxs(502)
    dados.q, dados.I, dados.sI = np.loadtxt(entrada, usecols=(0,1,2), unpack=True)
    for i in range(502):
        try:
            dados.lnI[i] = np.log(dados.I[i])
        except:
            dados.lnI[i] = -12 #min(dados.lnI)
            
    return dados

############################################################################################


########################### Faz o gráfico do ajuste linear. ###############################
###########################################################################################

def PlotAbs(qc, I1, I2):

    plt.plot(qc, I1, ls='-', lw=0.5, marker='.', markersize=1, color='#bb0022', label=u'Dados X014')
    plt.plot(qc, I2, ls='-', lw=0.5, marker='', markersize=0.2, color='#0077cc', label=u'Dados X010')
    plt.legend(loc='upper right')
    plt.grid(True)
    plt.xlabel(r'$q_{calibrado} \quad (\AA^{-1})$')
    plt.ylabel(r'$Intensidade$')
    
    #plt.xticks(np.arange(0, 0.40, 0.05))
    #plt.yticks(np.arange(0, 0.40, 0.05))
    plt.tick_params(axis='x', labelsize=8)
    plt.tick_params(axis='y', labelsize=8)
    plt.title(u'Intensidade calibrada')
    plt.savefig('EscCalib.pdf')
    plt.show()

    return 0

##############################################################################################



############################################################################################

def CorrectAbs(a, b, Am, TrAm, fr, thAm, Cap, TrCap, Water_cor, Norm, saida):
    saida = saida + ".dat"
    Am_abs = Saxs(502)

    # Correção da escala q.
    Am_abs.q = a*Am.q + b

    # Correção da intensidade pelo capilar e pelo solvente.
    Am_abs.I = (Am.I/TrAm - Cap.I/TrCap)/thAm - (1-fr)*Water_cor.I

    # Normalização para escala absoluta.
    Am_abs.I = Am_abs.I/Norm

    # Cálculo da nova incerteza.
    Am_abs.sI = np.sqrt((Am.sI/(TrAm*thAm))**2 + (Cap.sI/(TrCap*thAm))**2 + ((1-fr)*Water_cor.sI)**2)/Norm

    with open(saida, 'w') as arq:
        arq.write("# Arquivo de dados corrigidos\n")
        arq.write("# q\t\t I\t\t sigI\n")
        for i in range(502):
            arq.write("%.6e\t %.6e\t %.6e\n" % (Am_abs.q[i], Am_abs.I[i], Am_abs.sI[i]))

    for i in range(502):
        if(Am_abs.I[i] > 0):
            Am_abs.lnI[i] = np.log(Am_abs.I[i])
        else:
            Am_abs.lnI[i] = -12

    return Am_abs

                  
############################################################################################    

################################## PROGRAMA PRINCIPAL ######################################
############################################################################################

# Define o arquivo de parâmetros.
entrada = input("Entre com o arquivo de parâmetros:")
saida = input("Entre com o nome do arquivo de saída (dados corrigidos):")

# Importa os parâmetros.
a, b, ArqAm, TrAm, fr, thAm, ArqCap, TrCap, ArqWater_cor, Norm = ImportParameter(entrada)

#saida = ArqAm.strip('../')
#saida = saida.split('.')[0] + "_abs"

# Importa os dados SAXS.
Am = ImportData(ArqAm)
Cap = ImportData(ArqCap)
Water_cor = ImportData(ArqWater_cor)


# Corrige os dados e salva em arquivo.
Am_abs = CorrectAbs(a, b, Am, TrAm, fr, thAm, Cap, TrCap, Water_cor, Norm, saida)

# Faz o gráfico do ajuste de calibração.
#PlotAbs(dados1.q, dados1.lnI, dados2.lnI)



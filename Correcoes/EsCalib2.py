import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import linregress


############### Define uma classe com o formato dos dados do SAXS. #########################
############################################################################################

class Saxs():
    def __init__(self, Nq):
        self.q = np.zeros(Nq)
        self.I = np.zeros(Nq)
        self.sq = np.zeros(Nq)
        self.lnI = np.zeros(Nq)
    
############################################################################################


###################### Importa os parâmetros e nomes de arquivos. ##########################
############################################################################################

def ImportParameter(arquivo):
    # Extrai os dados do arquivo ignorando as linhas comentadas.
    with open(arquivo, 'r') as f:
        lines = []
        for linha in f:
            if (linha[0] != '#' and linha[0] != '\n' and linha[0] != ' ' and linha[0] != '\t'):
                lines.append(linha)

    # Verifica se o número de linhas é compatível com o esperado, interrompe o processo se não.
    if (len(lines) != 7):
        print("Problema com o arquivo de parâmetros!\n")
        

    # Obtêm os parâmetros e retorna.
    aux = lines[0].split()
    a = float(aux[2])

    aux = lines[1].split()
    b = float(aux[2])

    arqA = str(lines[2].strip('\n'))

    TrA = float(lines[3])

    th = float(lines[4])

    arqC = str(lines[5].strip('\n'))

    TrC = float(lines[6])
    
    return (a, b, arqA, TrA, th, arqC, TrC)

############################################################################################


################## Importa os dados de média do já computados. #############################
############################################################################################

def ImportData(entrada):
    dados = Saxs(502)
    dados.q, dados.I, dados.sq = np.loadtxt(entrada, usecols=(0,1,2), unpack=True)
    for i in range(502):
        try:
            dados.lnI[i] = np.log(dados.I[i])
        except:
            dados.lnI[i] = -12 #min(dados.lnI)
            
    return dados

############################################################################################


########################### Faz o gráfico do ajuste linear. ###############################
###########################################################################################

def PlotCalib(qc, I1, I2):

    plt.plot(qc, I1, ls='-', lw=0.5, marker='.', markersize=1, color='#bb0022', label=u'Dados X014')
    plt.plot(qc, I2, ls='-', lw=0.5, marker='', markersize=0.2, color='#0077cc', label=u'Dados X010')
    plt.legend(loc='upper right')
    plt.grid(True)
    plt.xlabel(r'$q_{calibrado} \quad (\AA^{-1})$')
    plt.ylabel(r'$Intensidade$')
    
    #plt.xticks(np.arange(0, 0.40, 0.05))
    #plt.yticks(np.arange(0, 0.40, 0.05))
    plt.tick_params(axis='x', labelsize=8)
    plt.tick_params(axis='y', labelsize=8)
    plt.title(u'Intensidade calibrada')
    plt.savefig('EscCalib.pdf')
    plt.show()

    return 0

##############################################################################################



############################################################################################

def Corrector(a, b, A, TrA, th, C, TrC, saida):
    saida = saida + ".dat"
    Acorr = Saxs(502)
    
    Acorr.q = a*A.q + b

    Acorr.I = (A.I/TrA - C.I/TrC)/th

    Acorr.sq = np.sqrt((A.sq/TrA)**2 + (C.sq/TrC)**2)/th

    with open(saida, 'w') as arq:
        arq.write("# Arquivo de dados corrigidos\n")
        arq.write("# q\t I\t sigI\t\n")
        for i in range(502):
            arq.write("%.6e\t %.6e\t %.6e\n" % (Acorr.q[i], Acorr.I[i], Acorr.sq[i]))
    try:
        Acorr.lnI = np.log(Acorr.I)
    except:
        Acorr.lnI = -12

    return Acorr

                  
############################################################################################    

################################## PROGRAMA PRINCIPAL ######################################
############################################################################################

# Define o arquivo de parâmetros.
entrada = input("Entre com o arquivo de parâmetros:")
saida = input("Entre com o nome do arquivo de saída (dados corrigidos):")

# Importa os parâmetros.
a, b, ArqA, TrA, th, ArqC, TrC = ImportParameter(entrada)

# Importa os dados SAXS.
A = ImportData(ArqA)
C = ImportData(ArqC)

# Corrige os dados e salva em arquivo.
Acorr = Corrector(a, b, A, TrA, th, C, TrC, saida)

# Faz o gráfico do ajuste de calibração.
#PlotCalib(dados1.q, dados1.lnI, dados2.lnI)


